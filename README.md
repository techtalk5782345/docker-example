# Hello World Docker Example

This is a simple hello world docker example using Java and Spring Boot.

## Pre-requisite

Ensure you have the following prerequisites installed before setting up the project:

- [Docker](https://www.docker.com/get-started/)
- [Maven (for building the Spring Boot project)](https://maven.apache.org/download.cgi)

## Getting Started

Follow these steps to get started with the docker-example Spring Boot project:

### Clone the Repository

```
git clone https://gitlab.com/techtalk5782345/docker-example
```

```
cd docker-example
```

### Build the Spring Boot Project

```
mvn clean install
```

### Build the Docker Image

```
docker build -t docker-example-with-springboot .
```

### Run the Docker Container

```
docker run -d -p 8081:8080 docker-example-with-springboot
```

### Access the Spring Boot application at

```
http://localhost:8081/hello-world
```