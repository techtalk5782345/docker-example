FROM openjdk:17-jdk-slim
MAINTAINER TechTalk
COPY /target/docker-example-0.0.1-SNAPSHOT.jar docker-example-0.0.1-SNAPSHOT.jar
ENTRYPOINT "java" "-jar" "docker-example-0.0.1-SNAPSHOT.jar"